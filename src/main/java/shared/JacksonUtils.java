package shared;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.IOException;
import java.nio.charset.Charset;

public class JacksonUtils {
    public static Parcel jsonToParcel(String jsonString) {
        try {
            return new ObjectMapper().readValue(jsonString, Parcel.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String parcelToJson(Parcel parcel) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new Hibernate5Module());
            return mapper.writeValueAsString(parcel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ByteBuf parcelToByteBuf(Parcel parcel) {
        return Unpooled.copiedBuffer(parcelToJson(parcel), Charset.defaultCharset());
    }

    public static ByteBuf parcelToByteBuf(Parcel parcel, Class<?> cls) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new Hibernate5Module());
            mapper.addMixIn(Object.class, cls);
            return Unpooled.copiedBuffer(mapper.writeValueAsString(parcel), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String msgToJsonString(Object msg) {
        ByteBuf bf = (ByteBuf) msg;
        return bf.toString(Charset.defaultCharset());
    }

    public static Parcel msgToParcel(Object msg) {
        return jsonToParcel(msgToJsonString(msg));
    }

}
