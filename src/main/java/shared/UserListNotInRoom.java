package shared;

import java.util.List;

public class UserListNotInRoom implements Parcel {
    public int roomId;
    public List<String> users;

    @Override
    public String toString() {
        return  "UserListNotInRoom: " +
                " NOT IN roomId=" + roomId +
                ", users=" + users +
                '}';
    }
}
