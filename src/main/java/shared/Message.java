package shared;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

public class Message implements Parcel {

    public Message() {
    }

    public Message(String msg, Integer roomID, String userName) {
        this.msg = msg;
        this.roomID = roomID;
        this.userName = userName;
    }

    private String msg;
    private Integer roomID;
    private String userName;
    private Date date = new Date();

    @JsonIgnore
    public boolean isSeen = false;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getRoomID() {
        return roomID;
    }

    public void setRoomID(Integer roomID) {
        this.roomID = roomID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return (!isSeen?"*** NEW ***\n\n":"")
                + msg + "\n\n"
                + "from '" + userName + '\''
                + " ,at: " + date
                + " ,room id: " + roomID;
    }
}
