package shared;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.netty.channel.Channel;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "userName",
        scope = User.class)

@Entity
@Table(name = "CHAT_USER")
public class User implements Parcel {

    public User() {
    }

    public User(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    @Id
    @Column(name = "USER_NAME",
            unique = true,
            nullable = false
    )
    @JsonView(Views.ForUserSend.class)
    private String userName;
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "USER_PASSWORD",
            nullable = false
    )
    //@JsonView(Views.ForUserSend.class)
    private String userPassword;
    public String getUserPassword() {
        return userPassword;
    }
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "userSet")
    @JsonView(Views.ForUserSend.class)
    Set<Room> roomSet = new HashSet<>();
    public Set<Room> getRoomSet() {
        return roomSet;
    }
    public void setRoomSet(Set<Room> roomSet) {
        this.roomSet = roomSet;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "roomAdmin")
    @JsonIgnore
    private Set<Room> roomAdminSet = new HashSet<>();
    public Set<Room> getRoomAdminSet() {
        return roomAdminSet;
    }
    public void setRoomAdminSet(Set<Room> roomAdminSet) {
        this.roomAdminSet = roomAdminSet;
    }

    @Override
    public String toString() {
        return userName;
    }

    public Room getRoomById(int id){
        return roomSet.stream().filter(room -> room.getRoomId() == id).findFirst().get();
    }
}
