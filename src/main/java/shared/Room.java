package shared;

import com.fasterxml.jackson.annotation.*;
import com.sun.istack.internal.NotNull;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "roomId",
        scope = Room.class)

@Entity
@Table(name = "ROOM")
public class Room implements Parcel {

    public Room() {
    }

    public Room(String roomName, User roomAdmin) {
        this.roomName = roomName;
        this.roomAdmin = roomAdmin;
    }

    @Column(name = "ROOM_NAME",
            nullable = false
    )
    @JsonView(Views.ForUserSend.class)
    @NotNull
    private String roomName;
    public String getRoomName() {
        return roomName;
    }
    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROOM_ID",
            unique = true,
            nullable = false,
            precision = 5
    )
    @JsonView(Views.ForUserSend.class)
    private int roomId;
    public int getRoomId() {
        return roomId;
    }
    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROOM_ADMIN", nullable = false)
    @JsonView(Views.ForUserSend.class)
    private User roomAdmin;
    public User getRoomAdmin() {
        return roomAdmin;
    }
    public void setRoomAdmin(User roomAdmin) {
        this.roomAdmin = roomAdmin;
    }

    @ManyToMany(cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH}
            )
    @JoinTable(name = "ROOM_USER",
            joinColumns = {@JoinColumn(name = "ROOM_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_NAME")}
    )
    @JsonView(Views.ForUserSend.class)
    private Set<User> userSet = new HashSet<>();
    public Set<User> getUserSet() {
        return userSet;
    }
    public void setUserSet(Set<User> userSet) {
        this.userSet = userSet;
    }

    @Transient
    @JsonIgnore
    public boolean isSeen = true;



    @Transient
    @JsonIgnore
    private ObservableList<Message> messageList = FXCollections.observableArrayList();
    public ObservableList<Message> getMessageList() {
        return messageList;
    }
    public void setMessageList(List<Message> messageList) {
        this.messageList = FXCollections.observableList(messageList);
    }

    @Override
    public String toString() {
        return roomName
                + " (" + roomId + ")"
                + " admin: '" + roomAdmin.getUserName() + "'";
    }

    //@Transient
    //@JsonIgnore
    //final public ChannelGroup channelsMap = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
}
