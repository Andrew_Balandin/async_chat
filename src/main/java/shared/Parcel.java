package shared;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Parcel_type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = User.class, name = "User"),
        @JsonSubTypes.Type(value = UserListNotInRoom.class, name = "UserListNotInRoom"),
        @JsonSubTypes.Type(value = UserToRoom.class, name = "UserToRoom"),
        @JsonSubTypes.Type(value = NewUser.class, name = "NewUser"),
        @JsonSubTypes.Type(value = Message.class, name = "Message"),
        @JsonSubTypes.Type(value = UserFailedAuthentication.class, name = "UserFailedAuthentication"),
        @JsonSubTypes.Type(value = AccessDeniedResponse.class, name = "AccessDeniedResponse"),
        @JsonSubTypes.Type(value = Room.class, name = "Room"),
})

public interface Parcel {

}
