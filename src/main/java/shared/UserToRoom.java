package shared;

public class UserToRoom implements Parcel {

    public UserToRoom() {
    }

    public UserToRoom(String userName, int roomId) {
        this.userName = userName;
        this.roomId = roomId;
    }

    private String userName;
    private int roomId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
}
