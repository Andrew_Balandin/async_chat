package tmp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TMPGui extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //ClientRoomManager.getInstance().channel = new ChatClient("127.0.0.1", 55555).start();
        Parent root = FXMLLoader.load(getClass().getResource("/ChatClient.fxml"));
        primaryStage.setTitle("Chat");
        primaryStage.setScene(new Scene(root));

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
