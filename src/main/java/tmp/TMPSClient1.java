package tmp;

import client.ChatClient;
import client.ClientRoomManager;
import shared.*;

public class TMPSClient1 {
    public static void main(String[] args) throws Exception {
        final User currentUser = new User();
        currentUser.setUserName("user1");
        currentUser.setUserPassword("pswd");
        System.out.println("Simple client is running...");
        ClientRoomManager.getInstance().channel = new ChatClient("127.0.0.1", 55555).start();

        ClientRoomManager.getInstance().sendParcel(currentUser);

        while(ClientRoomManager.getInstance().currentUser == null)
            Thread.sleep(100);

        Message message = new Message();
        message.msg = "new msg1";
        message.roomID = ((Room) ClientRoomManager.getInstance().currentUser.getRoomSet().toArray()[0]).getRoomId();
        message.userName = "user1";
        ClientRoomManager.getInstance().sendParcel(message);
    }
}