package tmp;

import server.ServerRoomManager;
import shared.*;

public class TMPHibernateJacksonTest {
    public static void main(String[] args) throws Exception{
        ServerRoomManager.getInstance().initChannelGroups();
        User user = new User();
        user.setUserName("user1");
        user.setUserPassword("pswd");
        Room r = new Room();
        System.out.println("---" + r.getRoomId());
        Room r1 = new Room();
        System.out.println("---" + r1.getRoomId());

        String toSend = JacksonUtils.parcelToJson(ServerRoomManager.getInstance().getUserByName(user.getUserName()));
        System.out.println(toSend);
        ServerRoomManager.getInstance().getUserByName("user1")
                .getRoomSet()
                .stream()
                .forEach(room -> System.out.println(room.getRoomName()));
        Parcel p = JacksonUtils.jsonToParcel(toSend);
        ((User)p).getRoomSet()
                .stream()
                .forEach(room -> System.out.println(room.getRoomName()));


        ServerRoomManager.getInstance().session.close();
        HibernateUtils.shutdown();
    }
}

