package tmp;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class TMPBeansTests extends Application{

    public static void main(String[] args) {


        Application.launch(args);
    }

    @Override
    public void start(Stage stage){
        Button button = new Button("new stage");
        button.setOnAction((ActionEvent e) -> {
            System.out.println("new stage");
            Stage stage1 = new Stage();
            stage1.setTitle("new stage");
            stage1.initModality(Modality.APPLICATION_MODAL);
            stage1.show();
            stage.close();
            }
        );
        Group group = new Group();
        group.getChildren().addAll(button);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.setTitle("test");
        stage.show();
    }
}
