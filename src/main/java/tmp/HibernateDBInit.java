package tmp;

import org.hibernate.Session;
import shared.*;

import java.util.HashSet;

public class HibernateDBInit
{
    public static void main( String[] args )
    {
        System.out.println("Maven + Hibernate + PostgresSQL");

        Session session = HibernateUtils.getSessionFactory().openSession();

        session.beginTransaction();

        User user = new User();
        user.setUserName("user1");
        user.setUserPassword("pswd");
        session.save(user);

        User user2 = new User();
        user2.setUserName("user2");
        user2.setUserPassword("pswd2");
        session.save(user2);

        User user3 = new User();
        user3.setUserName("user3");
        user3.setUserPassword("pswd3");
        session.save(user3);

        Room room = new Room();
        room.setRoomName("room1");
        room.setUserSet(new HashSet<>());
        room.getUserSet().add(user);
        room.getUserSet().add(user2);
        room.setRoomAdmin(user);
        session.save(room);

        Room room2 = new Room();
        room2.setRoomName("room2");
        room2.setUserSet(new HashSet<>());
        room2.getUserSet().add(user2);
        room2.getUserSet().add(user3);
        room2.setRoomAdmin(user2);
        session.save(room2);
        System.out.println("Done");
        //System.out.println("id: "+room2.getRoomId());


        //message = new shared.JacksonUtils.JsonToParcel(null).getMessage();

        //shared.Message m1 = (shared.Message)session.jsonToParcel(shared.Message.class, new Integer(1));

        //session.save(message);

        //session.delete(m1);

        session.getTransaction().commit();

        session.close();


        HibernateUtils.shutdown();



    }
}