package tmp;

import client.ChatClient;
import client.ClientRoomManager;
import shared.Message;
import shared.Room;
import shared.User;

public class TMPSClient3 {

    public static void main(String[] args) throws Exception {
        final User currentUser = new User();
        currentUser.setUserName("user3");
        currentUser.setUserPassword("pswd3");
        System.out.println("Simple client3 is running...");
        ClientRoomManager.getInstance().channel = new ChatClient("127.0.0.1", 55555).start();

        ClientRoomManager.getInstance().sendParcel(currentUser);

        while(ClientRoomManager.getInstance().currentUser == null)
            Thread.sleep(100);

        Message message = new Message();
        message.msg = "new msg3";
        message.roomID = ((Room) ClientRoomManager.getInstance().currentUser.getRoomSet().toArray()[0]).getRoomId();
        message.userName = "user3";
        ClientRoomManager.getInstance().sendParcel(message);




    }
}
