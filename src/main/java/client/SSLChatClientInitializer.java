package client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.json.JsonObjectDecoder;
import io.netty.handler.ssl.SslContext;

public class SSLChatClientInitializer extends ChannelInitializer<SocketChannel> {

    private final SslContext sslCtx;
    private final String server;
    private final int port;

    public SSLChatClientInitializer(SslContext sslCtx, String server, int port) {
        this.sslCtx = sslCtx;
        this.server = server;
        this.port = port;
    }

    @Override
    public void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        // Add SSL handler first to encrypt and decrypt everything.
        // In this example, we use a bogus certificate in the server side
        // and accept any invalid certificates in the client side.
        // You will need something more complicated to identify both
        // and server in the real world.
        pipeline.addLast(sslCtx.newHandler(ch.alloc(), server, port));

        // On top of the SSL handler, add the json line codec.
        ch.pipeline().addLast(new JsonObjectDecoder());

        // and then business logic.
        ch.pipeline().addLast(new ChatClientLoginHandler());

    }
}