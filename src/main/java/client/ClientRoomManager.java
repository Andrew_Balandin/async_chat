package client;

import io.netty.channel.Channel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import shared.*;

public class ClientRoomManager {

    public UserListNotInRoom userListNotInCurrentRoom;
    public Channel channel;
    private ObjectProperty<Room> currentRoom;

    private ObjectProperty<User> currentUser;
    private ObservableSet<Room> roomObservableSet = FXCollections.observableSet();

    private static ClientRoomManager ourInstance = new ClientRoomManager();

    public static ClientRoomManager getInstance() {
        return ourInstance;
    }

    private ClientRoomManager() {
    }

    public void sendParcel(Parcel parcel) throws Exception{
        channel.writeAndFlush(JacksonUtils.parcelToByteBuf(parcel)).sync();
        //TODO sync request with response
    }

    public void setCurrentRoom(Room newCurrentRoom){
        this.currentRoom.set(newCurrentRoom);
    }

    public void addRoom(Room room){
        currentUser.getValue().getRoomSet().add(room);
        roomObservableSet.add(room);
    }

    public void setCurrentUser(User user){
        currentUser.set(user);

        roomObservableSet.addAll(currentUser.getValue().getRoomSet());
    }

    public User getCurrentUser() {
        return currentUser.get();
    }

    public ObjectProperty<User> currentUserProperty() {
        return currentUser;
    }

    public ObservableSet<Room> getRoomObservableSet() {
        return roomObservableSet;
    }

    public Room getCurrentRoom() {
        return currentRoom.get();
    }

    public ObjectProperty<Room> currentRoomProperty() {
        return currentRoom;
    }
}
