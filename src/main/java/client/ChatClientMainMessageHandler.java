package client;

import io.netty.channel.ChannelHandlerContext;
import javafx.application.Platform;
import shared.Message;

public class ChatClientMainMessageHandler extends ChatClientMainHandler {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if(!(msg instanceof Message)){
            ctx.fireChannelRead(msg);
            return;
        }

        Message message = (Message) msg;

        ClientRoomManager.getInstance()
                .getCurrentUser()
                .getRoomById(message.getRoomID())
                .getMessageList()
                .add(message);

        if(ClientRoomManager.getInstance().getCurrentRoom() == null ||
                ClientRoomManager.getInstance().getCurrentRoom().getRoomId() != message.getRoomID()) {

            ClientRoomManager.getInstance()
                    .getCurrentUser()
                    .getRoomById(message.getRoomID())
                    .isSeen = false;
        }
//gui
        System.out.println(message);
        ctx.fireChannelReadComplete();
    }
}
