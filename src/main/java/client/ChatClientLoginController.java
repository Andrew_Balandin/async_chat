package client;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import shared.NewUser;
import shared.User;

public class ChatClientLoginController implements ChatController {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private TextField userNameTextField;

    @FXML
    private PasswordField userPasswordTextField;

    @FXML
    private Button loginButton;

    @FXML
    private Button newUserButton;

    @FXML
    public void handleNewUser() throws Exception{
        this.setDisableUserPswd(true);
        NewUser newUser = new NewUser();
        newUser.setUserName(userNameTextField.getText());
        newUser.setUserPassword(userPasswordTextField.getText());
        ClientRoomManager.getInstance().sendParcel(newUser);
    }

    @FXML //TODO handle exceptions
    public void handleLogin() throws Exception{
        this.setDisableUserPswd(true);
        User currentUser = new User();
        currentUser.setUserName(userNameTextField.getText());
        currentUser.setUserPassword(userPasswordTextField.getText());
        ClientRoomManager.getInstance().sendParcel(currentUser);
    }

    public void setDisableUserPswd(boolean value){
        newUserButton.setDisable(value);
        loginButton.setDisable(value);
        userNameTextField.setDisable(value);
        userPasswordTextField.setDisable(value);
    }
}
