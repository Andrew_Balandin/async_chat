package client;

import javafx.scene.control.ListCell;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import shared.Room;

public class FontRoomCell extends ListCell<Room> {
    @Override
    public void updateItem(Room item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            if(!item.isSeen && item != ClientRoomManager.getInstance().getCurrentRoom()){
                setFont(Font.font(Font.getDefault().getName(), FontWeight.BLACK, -1));
                setText(item.toString());
            }
            else{
                setFont(Font.getDefault());
                setText(item.toString());
            }
        }
    }
}