package client;

import javafx.scene.control.ListCell;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import shared.Message;

public class FontMessageCell extends ListCell<Message> {

   @Override
    public void updateItem(Message item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null && ClientRoomManager.getInstance().getCurrentRoom().getRoomId() != item.getRoomID()) {
            if(!item.isSeen){
                setFont(Font.font(Font.getDefault().getName(), FontWeight.BLACK, -1));
                setText(item.toString());
            }
            else{
                setFont(Font.getDefault());
                setText(item.toString());
            }
        }
    }
}

