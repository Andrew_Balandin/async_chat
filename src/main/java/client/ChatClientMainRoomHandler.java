package client;

import io.netty.channel.ChannelHandlerContext;
import javafx.application.Platform;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import shared.Room;

public class ChatClientMainRoomHandler extends ChatClientMainHandler {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!(msg instanceof Room)) {
            ctx.fireChannelRead(msg);
            return;
        }
        Room newRoom = (Room) msg;
        newRoom.isSeen = false;
        ClientRoomManager.getInstance().addRoom(newRoom);

    }
}