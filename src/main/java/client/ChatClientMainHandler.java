package client;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import shared.JacksonUtils;
import shared.Parcel;

//@ChannelHandler.Sharable
public class ChatClientMainHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //System.out.println(new JSONObject(JacksonUtils.msgToJsonString(msg)).toString(4));
        System.out.println(JacksonUtils.msgToJsonString(msg));
        Parcel parcel = JacksonUtils.msgToParcel(msg);
        ctx.fireChannelRead(parcel);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.channel().close().sync();
        System.exit(1);
        //super.exceptionCaught(ctx, cause);
    }
}
