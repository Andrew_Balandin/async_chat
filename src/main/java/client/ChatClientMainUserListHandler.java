package client;

import io.netty.channel.ChannelHandlerContext;
import server.ChatServerMainHandler;
import shared.UserListNotInRoom;

public class ChatClientMainUserListHandler extends ChatServerMainHandler {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!(msg instanceof UserListNotInRoom)) {
            ctx.fireChannelRead(msg);
            return;
        }
        ClientRoomManager.getInstance().userListNotInCurrentRoom = (UserListNotInRoom) msg;

        //((ChatClientController)ChatClientGUI.chatController).choiceDialogAddUser
        //super.channelRead(ctx, msg);
    }
}
