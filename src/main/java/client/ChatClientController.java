package client;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import shared.*;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class ChatClientController implements ChatController, Initializable{

    ChoiceDialog<String> choiceDialogAddUser;

    @FXML
    private ListView<User> userInRoom;

    @FXML
    private TextArea newMessageTextArea;

    @FXML
    ListView<Room> roomListView;

    @FXML
    ListView<Message> messageListView;

    @FXML
    private Button sendMessageButton;

    @FXML
    private Button newRoomButton;

    @FXML
    private Button addUserButton;

    @FXML
    private Label currentRoomLabel;

    @FXML
    public void handleNewRoom() throws Exception{

        TextInputDialog dialog = new TextInputDialog("new room");
        dialog.setTitle("New room");
        dialog.setHeaderText("Create new room name!");
        dialog.setContentText("Please enter name of new room:");
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()){
            System.out.println("New room: " + result.get());
            Room newRoom = new Room();
            newRoom.setRoomName(result.get());
            User newRoomAdmin = new User();
            newRoomAdmin.setUserName(ClientRoomManager.getInstance().getCurrentUser().getUserName());
            newRoom.setRoomAdmin(newRoomAdmin);
            ClientRoomManager.getInstance().sendParcel(newRoom);
        }
    }

    @FXML
    public void handleAddUserToRoom() throws Exception{

        ClientRoomManager crm = ClientRoomManager.getInstance();
        UserListNotInRoom userListNotInRoom = new UserListNotInRoom();
        userListNotInRoom.roomId = crm.getCurrentRoom().getRoomId();
        crm.sendParcel(userListNotInRoom);


        choiceDialogAddUser = new ChoiceDialog<>();
        choiceDialogAddUser.setTitle("Add user to " + crm.getCurrentRoom().getRoomName());
        choiceDialogAddUser.setHeaderText("Add user to " + crm.getCurrentRoom().getRoomName());
        choiceDialogAddUser.setContentText("Choose user to add: ");
        Optional<String> result = choiceDialogAddUser.showAndWait();
            if (result.isPresent()) {
                System.out.println("Add user: '" +
                        result.get() +
                        "' to room: " +
                        crm.getCurrentRoom().getRoomName());
                crm.sendParcel(new UserToRoom(result.get(), crm.getCurrentRoom().getRoomId()));
                User newUserInRoom = new User();
                newUserInRoom.setUserName(result.get());
                crm.getCurrentRoom().getUserSet().add(newUserInRoom);
                userInRoom.getItems().setAll(crm.getCurrentRoom().getUserSet());
                userInRoom.refresh();
            }

    }


    @FXML
    public void handleChangeRoom(){
        if(ClientRoomManager.getInstance().getCurrentRoom() != null){
            ClientRoomManager.getInstance().getCurrentRoom().isSeen = true;
            ClientRoomManager.getInstance().getCurrentRoom().getMessageList().forEach(message -> message.isSeen = true);
        }
        Room selectedRoom = roomListView.getSelectionModel().getSelectedItem();
        selectedRoom.isSeen = true;
        ClientRoomManager.getInstance().setCurrentRoom(selectedRoom);
        currentRoomLabel.setText(selectedRoom.toString());
        messageListView.setItems(selectedRoom.getMessageList());
        roomListView.refresh();
        userInRoom.getItems().setAll(selectedRoom.getUserSet());
        if(selectedRoom.getRoomAdmin()
                .getUserName()
                .equals( ClientRoomManager.getInstance().getCurrentUser().getUserName() ))
            addUserButton.setDisable(false);
        else
            addUserButton.setDisable(true);
        sendMessageButton.setDisable(false);
    }

    @FXML
    public void handleSendNewMessage() throws Exception{
        Message newMessage = new Message(
                newMessageTextArea.getText(),
                ClientRoomManager.getInstance().getCurrentRoom().getRoomId(),
                ClientRoomManager.getInstance().getCurrentUser().getUserName()
        );

        ClientRoomManager.getInstance()
                .channel
                .writeAndFlush(JacksonUtils.parcelToByteBuf(newMessage, MixInByUserPswd.class)).sync();
        newMessageTextArea.clear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {



        roomListView.getItems().addAll(ClientRoomManager.getInstance().getRoomObservableSet());
        roomListView.setCellFactory((ListView<Room> list) -> new FontRoomCell());
        //messageListView.setCellFactory((ListView<Message> list) -> new FontMessageCell());
    }


}

