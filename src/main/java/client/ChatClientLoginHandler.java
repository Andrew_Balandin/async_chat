package client;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.json.JSONObject;
import shared.User;
import shared.UserFailedAuthentication;

import static shared.JacksonUtils.msgToJsonString;
import static shared.JacksonUtils.msgToParcel;

//@ChannelHandler.Sharable
public class ChatClientLoginHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("Connection established with " + ctx.channel().remoteAddress());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println((new JSONObject(msgToJsonString(msg))).toString(4));

        User user = (User)msgToParcel(msg);
        if(user instanceof UserFailedAuthentication){
            throw new Exception("Login failed");
        }
        ClientRoomManager crm = ClientRoomManager.getInstance();
        crm.setCurrentUser(user);



        crm.getCurrentUser().getRoomSet()
                .stream()
                .forEach(room -> System.out.println(room.getRoomName()
                        + ": \n" + room.getUserSet().toArray().toString()));
        System.out.println("Authenticated!!!");

        crm.channel.pipeline().removeLast();
        crm.channel.pipeline().addLast(new ChatClientMainHandler());
        crm.channel.pipeline().addLast(new ChatClientMainMessageHandler());
        crm.channel.pipeline().addLast(new ChatClientMainRoomHandler());
        crm.channel.pipeline().addLast(new ChatClientMainUserListHandler());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //((ChatClientLoginController)ClientRoomManager.getInstance().chatController).setDisableUserPswd(false);
        ((ChatClientLoginController)ChatClientGUI.chatController).setDisableUserPswd(false);
        cause.printStackTrace();
        ctx.fireChannelReadComplete();
    }
}
