package client;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.SetChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import shared.Room;
import shared.User;
import shared.UserListNotInRoom;

public class ChatClientGUI extends Application {

    public static ChatController chatController;

    public static Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception{
        stage = primaryStage;
        ClientRoomManager crm = ClientRoomManager.getInstance();
        crm.channel = new ChatClient("127.0.0.1", 55555).start();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/LoginClient.fxml"));
        Parent root = loader.load();
        //ClientRoomManager.getInstance().chatController = loader.getController();
        ChatClientGUI.chatController = loader.getController();
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        primaryStage.setOnCloseRequest(t -> {
            crm.channel.eventLoop().shutdownGracefully();
            Platform.exit();
            System.exit(0);
        });

        crm.currentUserProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                FXMLLoader newloader = new FXMLLoader(getClass().getResource("/ChatClient.fxml"));
                try {
                    Parent newRoot = loader.load();
                    chatController = loader.getController();
                    stage.setScene(new Scene(newRoot));
                    stage.setTitle("Asynchronous chat: " + ClientRoomManager.getInstance().getCurrentUser());
                } catch (Exception e) {
                }
            }
            //login(newValue);
        });

        crm.currentRoomProperty().addListener((observable, oldValue, newValue) -> {
            return;
        });

        crm.getRoomObservableSet().addListener((SetChangeListener<Room>) change -> {
            Room newRoom = change.getElementAdded();

        });


    }

    public void login(User user){
        Platform.runLater(() -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ChatClient.fxml"));
            try{
                Parent root = loader.load();
                chatController = loader.getController();
                stage.setScene(new Scene(root));
                stage.setTitle("Asynchronous chat: " + user.getUserName());
            }
            catch (Exception e){
            }
        });
    }

    public void newRoom(){
        Platform.runLater(() -> {

            ((ChatClientController) ChatClientGUI.chatController)
                    .roomListView
                    .refresh();
        });
    }

    public void newMessage(){
        Platform.runLater(() -> {

                ((ChatClientController)ChatClientGUI.chatController)
                        .roomListView
                        .refresh();
        });
    }

    public void userListNotInRoomInit(UserListNotInRoom userListNotInRoom){
        Platform.runLater(()->
                ((ChatClientController)chatController)
                        .choiceDialogAddUser
                        .getItems()
                        .setAll(userListNotInRoom.users)
        );
    }

    public static void main(String[] args) {
        launch(args);
    }
}
