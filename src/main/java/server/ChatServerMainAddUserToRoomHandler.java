package server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import shared.JacksonUtils;
import shared.MixInByUserPswd;
import shared.Room;
import shared.UserToRoom;

@ChannelHandler.Sharable
public class ChatServerMainAddUserToRoomHandler extends ChatServerMainHandler {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (!(msg instanceof UserToRoom)) {
            ctx.fireChannelRead(msg);
            return;
        }

        ServerRoomManager srm = ServerRoomManager.getInstance();
        UserToRoom userToRoom = (UserToRoom) msg;
        Room room = srm.addUserToRoom(userToRoom.getUserName(), userToRoom.getRoomId());

        srm.usersOnline
                .stream()
                .filter(channel ->
                        channel.attr(srm.userName).get().equals(userToRoom.getUserName()))
                .findFirst()
                .ifPresent(channel -> {
                    srm.channelsMap.get(userToRoom.getRoomId()).add(channel);
                    channel.writeAndFlush(JacksonUtils.parcelToByteBuf(room, MixInByUserPswd.class));
                });

        ctx.fireChannelReadComplete();
    }
}
