package server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shared.JacksonUtils;
import shared.Parcel;


public class ChatServerMainHandler extends ChannelInboundHandlerAdapter {

    private static Logger logger = LoggerFactory.getLogger(ChatServerMainHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
        logger.debug("Parcel from {} : \n{}",
                ctx.channel().remoteAddress(),
                JacksonUtils.msgToJsonString(msg));
        Parcel parcel = JacksonUtils.msgToParcel(msg);
        ctx.fireChannelRead(parcel);

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        //TODO handle exceptions
        // Close the connection when an exception is raised.
        logger.error("Exception:\n{}", cause.toString());
        //cause.printStackTrace();
        ctx.close();
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        logger.info("Connection was CLOSED to {}", ctx.channel().remoteAddress());
    }

}










