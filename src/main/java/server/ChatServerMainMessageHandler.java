package server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import shared.Message;

//@ChannelHandler.Sharable
public class ChatServerMainMessageHandler extends ChatServerMainHandler {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{

        if(!(msg instanceof Message)){
            ctx.fireChannelRead(msg);
            return;
        }
        Message newMessage = (Message) msg;

        ServerRoomManager.getInstance().sendParcelToRoom(newMessage, newMessage.getRoomID());
        ctx.fireChannelReadComplete();
        //  ReferenceCountUtil.release(msg);
    }
}
