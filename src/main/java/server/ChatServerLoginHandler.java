package server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shared.*;
import shared.exceptions.UserDoesNotExistException;

import java.net.InetAddress;

import static shared.JacksonUtils.parcelToByteBuf;


@ChannelHandler.Sharable
public class ChatServerLoginHandler extends ChannelInboundHandlerAdapter{

    private static Logger logger = LoggerFactory.getLogger(ChatServerLoginHandler.class);

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception{
        logger.info("Connection was OPENED to {}", ctx.channel().remoteAddress());
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        logger.info("Connection was CLOSED to {}", ctx.channel().remoteAddress());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
        logger.debug("Parcel from {} \n---\n{}\n---", ctx.channel().remoteAddress(),
                JacksonUtils.msgToJsonString(msg));
        User channelUser = (User) JacksonUtils.msgToParcel(msg);
        logger.info("User '{}' is attempting connection.", channelUser.getUserName());
        ctx.fireChannelRead(channelUser);
        //  ReferenceCountUtil.release(msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if(cause instanceof UserDoesNotExistException){
            logger.info("User '{}' does not exist.", cause.getMessage());
        }

        ctx.writeAndFlush(parcelToByteBuf(new UserFailedAuthentication())).sync();
        logger.info("'Authentication failed' parcel was send!");
        ctx.fireChannelReadComplete();
        cause.printStackTrace();
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        // Once session is secured, send a greeting and register the channel to the global channel
        // list so the channel received the messages from others.
        ctx.pipeline().get(SslHandler.class).handshakeFuture().addListener(
                (GenericFutureListener<Future<Channel>>) future -> {
                    //ctx.writeAndFlush(
                    logger.info("Session is protected by {} cipher suite.",
                                ctx.pipeline().get(SslHandler.class).engine().getSession().getCipherSuite());
                });
    }

    public void removeLoginAddMainHandlers(ChannelHandlerContext ctx){
        ctx.pipeline().remove("ChatServerLoginHandler");
        ctx.pipeline().remove("ChatServerLoginAuthHandler");
        ctx.pipeline().remove("ChatServerLoginNewUserHandler");
        ctx.pipeline().addLast(new ChatServerMainHandler());
        ctx.pipeline().addLast(new ChatServerMainMessageHandler());
        ctx.pipeline().addLast(new ChatServerMainRoomHandler());
        ctx.pipeline().addLast(new ChatServerMainAddUserToRoomHandler());
        ctx.pipeline().addLast(new ChatServerMainUserListHandler());
        logger.debug("Server handlers was changed.");
    }
}
