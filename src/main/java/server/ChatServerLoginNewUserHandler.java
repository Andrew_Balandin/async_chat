package server;

import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shared.JacksonUtils;
import shared.MixInByUserPswd;
import shared.NewUser;
import shared.User;


public class ChatServerLoginNewUserHandler extends ChatServerLoginHandler {

    private static Logger logger = LoggerFactory.getLogger(ChatServerLoginNewUserHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if(!(msg instanceof NewUser)){
            ctx.fireChannelRead(msg);
            return;
        }
        ServerRoomManager srm = ServerRoomManager.getInstance();
        logger.info("User '{}' is attempting to create.", ((User) msg).getUserName());
        User user = srm.addUser((NewUser) msg);
        ctx.writeAndFlush(JacksonUtils.parcelToByteBuf(user, MixInByUserPswd.class)).sync();
        ctx.channel().attr(srm.userName).set(user.getUserName());
        srm.usersOnline.add(ctx.channel());
        this.removeLoginAddMainHandlers(ctx);
        logger.info("New user '{}' was created.", user.getUserName());
        ctx.fireChannelReadComplete();
    }
}
