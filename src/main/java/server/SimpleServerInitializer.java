package server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.json.JsonObjectDecoder;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.LoggerFactory;

public class SimpleServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        //ch.pipeline().addLast(new LoggingHandler());
        ch.pipeline().addLast(new JsonObjectDecoder());
        ch.pipeline().addLast("ChatServerLoginHandler", new ChatServerLoginHandler());
        ch.pipeline().addLast("ChatServerLoginAuthHandler", new ChatServerLoginAuthHandler());
        ch.pipeline().addLast("ChatServerLoginNewUserHandler", new ChatServerLoginNewUserHandler());
    }
}
