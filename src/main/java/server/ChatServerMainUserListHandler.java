package server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shared.JacksonUtils;
import shared.UserListNotInRoom;

//@ChannelHandler.Sharable
public class ChatServerMainUserListHandler extends ChatServerMainHandler {

    private static Logger logger = LoggerFactory.getLogger(ChatServerMainUserListHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!(msg instanceof UserListNotInRoom)) {
            ctx.fireChannelReadComplete();
            logger.error("Unknown parcel!");
            throw new Exception("Unknown parcel!");
        }
        ServerRoomManager srm = ServerRoomManager.getInstance();
        UserListNotInRoom userListNotInRoom = (UserListNotInRoom) msg;
        userListNotInRoom.users = srm.getUserListNotInRoom(userListNotInRoom.roomId);
        ctx.channel().writeAndFlush(JacksonUtils.parcelToByteBuf(userListNotInRoom)).sync();
        logger.debug("Users: {} was send to {} '{}'.",
                userListNotInRoom,
                ctx.channel().remoteAddress(),
                ctx.channel().attr(srm.userName).get());
        ctx.fireChannelReadComplete();
        //super.channelRead(ctx, msg);
    }
}
