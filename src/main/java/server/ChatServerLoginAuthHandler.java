package server;

import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shared.MixInByUserPswd;
import shared.NewUser;
import shared.Room;
import shared.User;

import java.util.Set;

import static shared.JacksonUtils.parcelToByteBuf;

public class ChatServerLoginAuthHandler extends ChatServerLoginHandler {

    private static Logger logger = LoggerFactory.getLogger(ChatServerLoginHandler.class);

    private static String roomSetToString(Set<Room> roomSet) {
        String retVal = "";
        for(Room r: roomSet)
            retVal += r.getRoomName() + ": " + r.getRoomId() + "\n";
        return retVal;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if(!(msg instanceof User)){
            ctx.fireChannelRead(msg);
            return;
        }
        ServerRoomManager srm = ServerRoomManager.getInstance();
        User unAuthenticatedUser = ((User)msg);
        User authenticatedUser = srm.isAuthenticated(unAuthenticatedUser);
        logger.info("User '" + unAuthenticatedUser.getUserName() + "' is attempting to authenticate");
        if(authenticatedUser != null){
            logger.info("User: '{}'. Authentication was passed, user rooms:\n{}",
                    authenticatedUser.getUserName(),
                    roomSetToString(authenticatedUser.getRoomSet()));

            ctx.writeAndFlush(parcelToByteBuf(authenticatedUser, MixInByUserPswd.class)).sync();
            authenticatedUser.getRoomSet()
                    .forEach(room -> srm.channelsMap
                            .get(room.getRoomId()).add(ctx.channel())
                            );
            srm.usersOnline.add(ctx.channel());
            ctx.channel().attr(srm.userName).set(authenticatedUser.getUserName());
            this.removeLoginAddMainHandlers(ctx);
            ctx.fireChannelReadComplete();
        }
        else{
            logger.info("User: '{}'. Authentication failed!", unAuthenticatedUser.getUserName());
            throw new Exception("Authentication failed!");
        }
    }
}

