package server;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shared.*;
import shared.exceptions.UserDoesNotExistException;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

import static shared.PasswordHashUtils.createHash;
import static shared.PasswordHashUtils.validatePassword;

public class ServerRoomManager {

    private static Logger logger = LoggerFactory.getLogger(ServerRoomManager.class);

    private static ServerRoomManager ourInstance = new ServerRoomManager();

    public static ServerRoomManager getInstance() {
        return ourInstance;
    }

    private ServerRoomManager() {
    }


    public final AttributeKey<String> userName = AttributeKey.valueOf("userName");
    public final Map<Integer, ChannelGroup> channelsMap = new HashMap<>();
    public final ChannelGroup usersOnline = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    public Session session = HibernateUtils.getSessionFactory().openSession();

    public void initChannelGroups() throws Exception{
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Room.class);
        Root<Room> rootRoom = criteriaQuery.from(Room.class);
        criteriaQuery.select(rootRoom);
        criteriaQuery.distinct(true);
        Query query = session.createQuery(criteriaQuery);
        List<Room> Rooms = query.getResultList();
        Rooms.forEach(room -> channelsMap.put(room.getRoomId(),
                new DefaultChannelGroup(GlobalEventExecutor.INSTANCE)));
        session.getTransaction().commit();
        logger.debug("Channel groups was initialized!\n{}", channelsMap.toString());
        //channelsMap.keySet().stream().forEach(c-> System.out.println(c));
    }

    public User getUserByName(String userName) throws Exception{
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        User user = session.get(User.class, userName);

        if(user != null){
            user.getRoomSet().forEach(room -> room.getUserSet()
                    .forEach(userInRoom -> userInRoom.getUserName())
            );
        }
        else{
            session.getTransaction().commit();
            throw new UserDoesNotExistException(userName);
        }


        logger.debug("User '{}' was load.", user.getUserName());
        return user;
    }

    public User isAuthenticated(User someUser) throws Exception{
        User authenticatedUser  = getUserByName(someUser.getUserName());
        if(validatePassword(someUser.getUserPassword(), authenticatedUser.getUserPassword()))
            return authenticatedUser;
        else
            return null;
    }

    public User addUser(NewUser newUser) throws Exception{
        session = HibernateUtils.getSessionFactory().openSession();
        User user = new User();
        user.setUserName(newUser.getUserName());
        user.setUserPassword(createHash(newUser.getUserPassword()));
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        logger.debug("User '{}' was added.", user.getUserName());
        return user;
    }

    public Room getRoomById(int roomId) throws Exception{
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        Room room = session.get(Room.class, roomId);

        if(room != null)
            room.getUserSet().forEach(user -> user.getUserName()
                    //.forEach(userInRoom -> userInRoom.getUserName())
            );
        session.getTransaction().commit();
        logger.debug("Room '{}:{}' was load.", room.getRoomName(), room.getRoomId());
        return room;
    }

    public Room addRoom(Room newRoom) throws Exception {
        User newRoomAdmin = getUserByName(newRoom.getRoomAdmin().getUserName());
        newRoom.setRoomAdmin(newRoomAdmin);
        Set<User> newRoomUserSet = new HashSet<>();
        newRoomUserSet.add(newRoomAdmin);
        newRoom.setUserSet(newRoomUserSet);
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(newRoom);
        session.getTransaction().commit();
        channelsMap.put(newRoom.getRoomId(), new DefaultChannelGroup(GlobalEventExecutor.INSTANCE));
        logger.debug("Room '{}' was added with id:{} .", newRoom.getRoomName(), newRoom.getRoomId());
        return newRoom;
    }

    public void sendParcelToRoom(Parcel parcel, Integer roomId) throws Exception {
        ServerRoomManager.getInstance()
                .channelsMap
                .get(roomId)
                .writeAndFlush(JacksonUtils.parcelToByteBuf(parcel, MixInByUserPswd.class)).sync();
        logger.debug("Parcel:\n'{}'\nwas send.", parcel);
    }

    //TODO check authorization - is user-sender admin in room?
    public Room addUserToRoom (String userName, int roomId) throws Exception{
        session = HibernateUtils.getSessionFactory().openSession();
        User user = getUserByName(userName);
        Room room = getRoomById(roomId);
        room.getUserSet().add(user);
        session.beginTransaction();
        session.update(room);
        session.getTransaction().commit();
        logger.debug("User '{}' was added to room '{}:{}'",
                user.getUserName(), room.getRoomName(), room.getRoomId());
        return getRoomById(roomId);
    }

    public List<String> getUserListNotInRoom(int roomId){
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        String queryString =
                "SELECT chat_user.user_name\n" +
                "FROM chat_user\n" +
                "WHERE chat_user.user_name NOT IN\n" +
                "(SELECT \n" +
                "  chat_user.user_name AS USER_NAME\n" +
                "FROM public.chat_user LEFT OUTER JOIN public.room_user " +
                "ON chat_user.user_name = room_user.user_name\n" +
                "WHERE room_user.room_id = " + roomId +")";
        Query query = session.createNativeQuery(queryString);
        List<String> retVal = query.getResultList();
        session.getTransaction().commit();
        logger.debug("User list was loaded! Users: {} are not in room {}.", retVal.toString(), roomId);
        return retVal;
    }
}
