package server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import shared.Room;

public class ChatServerMainRoomHandler extends ChatServerMainHandler {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if(!(msg instanceof Room)){
            ctx.fireChannelRead(msg);
            return;
        }
        ServerRoomManager srm = ServerRoomManager.getInstance();
        Room newRoom = (Room)msg;
        srm.addRoom(newRoom);
        srm.channelsMap.get(newRoom.getRoomId()).add(ctx.channel());
        srm.sendParcelToRoom(newRoom, newRoom.getRoomId());
        ctx.fireChannelReadComplete();

        //super.channelRead(ctx, msg);
    }
}
